case $- in
    *i*) ;;
      *) return;;
esac

HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000

shopt -s histappend
shopt -s checkwinsize

PS1='\u@\h \w$ '

if ! shopt -oq posix && [ -f /etc/bash_completion ]; then
  . /etc/bash_completion
fi
